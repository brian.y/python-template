FROM python:3

WORKDIR /usr/src/app

# Install dependencies
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

# Run the application
COPY . .
CMD [ "python", "./hello-world.py" ]
